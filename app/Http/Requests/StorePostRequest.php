<?php

namespace App\Http\Requests;

use Dotenv\Validator;
use Dotenv\Validator as ValidatorAlias;
use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Http\Response;
use \Illuminate\Validation\ValidationException;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:4|string',
            'body' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Họ và tên',
            'body' => 'nội dung'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được để trống ten: attribute',
            'body.required' => 'Không được để trống: attribute',
            'name.min'=> 'ten phai lon hon 4 ki tu',
//          'body.email' => 'phai dung dinh dang email',
            'name.string'=> 'khong duoc co so: attributes',
        ];
    }
    /**
     * @param ValidatorAlias $validatior
     * @return void
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new Response([
            'errors' =>$validator->errors()
        ],Response::HTTP_UNPROCESSABLE_ENTITY);

        throw (new ValidationException($validator,$response));
    }

}
